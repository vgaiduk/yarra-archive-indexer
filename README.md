yarra-archive-indexer is an indexing service that recursively scans the configured directories to discover measurement data files from Siemens MRI scanners and stores its metadata in the database. The service can be run as cron job on one of the Linux servers to update the database as new files were archived or old files were removed; only new or updated files are re-read unless full database rebuild is requested.

The following tags are retrieved from measurement files and stored in the database:

- Patient Name
- Patient ID (MRN)
- Protocol Name
- Acquisition Time & Date
- MR System

If a Yarra .task file exists with the same filename, additional information is read from the .task file and stored in the database:

- Yarra Reconstruction Mode
- Accession Number
- Selected Yarra Server

# Contributors

Kaveh Vahedipour: [protpp C++ library](https://bitbucket.org/kvahed/protpp)

Vadim Gaiduk, Tobias Block
 

# License Information #
The Yarra framework is provided free of charge for use in research applications. It must not be used 
for diagnostic applications. The author takes no responsibility of any kind for the accuracy or integrity 
of the created data sets. No data created using the framework should be used to make diagnostic decisions. The Yarra framework, including all of its software components, comes without any warranties. 
Operation of the software is solely on the user’s own risk. The author takes no responsibility for damage 
or misfunction of any kind caused by the software, including permanent damages to MR systems, temporary 
unavailability of MR systems, and loss or corruption of research or patient data. 
This applies to all software components included in the Yarra software package.

The source is released under the GNU General Public License, GPL (http://www.gnu.org/copyleft/gpl.html). 
The source code is provided without warranties of any kind.

Usage
=====

yarra-archive-indexer [options] [<directories-to-index>]

Allowed options:
----------------

**General options:**

`-h`, `--help`                         
produce help message

 `-I`, `--index` *arg* (default = */<path-to-executable>/yarra-archive-indexer.db*)  
index file name

`keep`, `k` *arg* (default = *30*)  
days to keep an index entry before removing

`-E`, `--ext` *arg* (default = *.dat*)    
measurement file extension

`--reset`                               
reset the index database and rebuild index

`-C`, `--config` *arg*                   
config file name

**Logging options:**

`--logtostderr` *arg* (default = *0*)       
Log messages to stderr instead of logfiles. Note: you can set binary 
flags to true by specifying 1, true, or yes (case insensitive). Also, you can 
set binary flags to false by specifying 0, false, or no (again, case insensitive)

`--stderrthreshold` *arg* (default = *2*)   
Copy log messages at or above this level to stderr in addition to 
logfiles. The numbers of severity levels INFO, WARNING, ERROR, and FATAL 
are 0, 1, 2, and 3, respectively.

`--minloglevel` *arg* (default = *0*)       
Log messages at or above this level. Again, the numbers of severity levels 
INFO, WARNING, ERROR, and FATAL are 0, 1, 2, and 3, respectively.

`--log_dir` *arg*                         
If specified, logfiles are written into this directory instead of the default logging directory.

Configuration file structure
==================

The configuration file has INI-like line-based syntax.

A line in the form:  

    name=value  

gives a value to an option. 

The # character introduces a comment that spans until the end of the line.  
Options are the same as listed above for the command line (used without --).

To provide directory paths to index, add one or more lines:

    index_dir = <path>