#!/usr/bin/env bash

rm -R build/
mkdir build && cd build
cmake .. -DCMAKE_CXX_COMPILER=g++-5 -DCMAKE_CC_COMPILER=gcc-5 -DBOOST_ROOT="/home/vadim/Projects-Cai2R/boost_1_59_0" -DCMAKE_MODULE_PATH="/home/vadim/Projects-Cai2R/wt/cmake/"
make
