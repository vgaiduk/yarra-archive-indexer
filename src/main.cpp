#include <stdio.h>
#include <stdlib.h>
#include <mntent.h>

#include <iostream>
#include <vector>
#include <chrono>

#include <Wt/Dbo/Dbo>
#include <Wt/Dbo/backend/Sqlite3>

#include <boost/filesystem.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/program_options.hpp>
#include <boost/exception/all.hpp>

#include <glog/logging.h>


#include "../setdcmtags/src/syngoWrapper.h"

#define STR(s) XSTR(s)
#define XSTR(s) #s


//TODO: move out of TagsLookupTable.cpp

#include <boost/regex.hpp>
#include <chrono>

std::string cleanStringValue (const std::string & value) {
    //The regex to remove leading and trailing spaces and quotes
    static auto remove_space = boost::regex("\\A\\s*\"|\\A\\s*|\"\\s*\\z|\\s*\\z");

    //The regex to replace array delimiter (quotes, spaces and a comma) to one used in DICOMs (backslash)
    static auto to_dicom_array = boost::regex("\"\\s*,\\s*\"");

    return boost::regex_replace(boost::regex_replace(value, remove_space, ""), to_dicom_array, "\\");
}

class MountPoints {
    std::string substFsTypes = "cifs,nfs";

public:
    MountPoints() {
        struct mntent *ent;
        FILE *aFile;

        aFile = setmntent("/proc/mounts", "r");
        if (aFile == NULL) {
            perror("setmntent");
            return;
        }
        while (NULL != (ent = getmntent(aFile))) {
            if (substFsTypes.find(ent->mnt_type) != std::string::npos) {
                mountPointEntries.emplace_back(ent->mnt_dir, ent->mnt_fsname);
            }
        }
        endmntent(aFile);
    }

    std::string substMountPath(std::string path) {
        for (const auto &mount_point : mountPointEntries) {
            if (path.find(mount_point.first) == 0) {
                return path.replace(0, mount_point.first.length(), mount_point.second);
            }
        }
    }

private:
    std::vector<std::pair<std::string, std::string>> mountPointEntries;
};



// A struct to be mapped to the db table
struct MeasFileMeta {
    static MountPoints mountPoints;

    //Time when an index entry has been seen or updated during the indexing
    std::time_t LastSeen;

    //The meas file attributes used in index indicating if the file shall be re-read
    std::string Filename;
    std::string Path;
    std::time_t LastWriteTime;

    //Meas file attributes read from the file tags
    std::string PatientName;
    std::string PatientID; 
    std::string ProtocolName;
    std::string AcquisitionTime;
    std::string AcquisitionDate;
    std::string MRSystem;
    std::string AccessionNumber;
    std::string SelectedYarraServer;


    // Mapping method for Wt::Dbo
    template<class Action>
    void persist(Action& a)
    {
        Wt::Dbo::field(a, LastSeen, "LastSeen");

        Wt::Dbo::field(a, Filename, "Filename");
        Wt::Dbo::field(a, Path, "Path");
        Wt::Dbo::field(a, LastWriteTime, "LastWriteTime");

        Wt::Dbo::field(a, PatientName, "PatientName");
        Wt::Dbo::field(a, PatientID, "PatientID");
        Wt::Dbo::field(a, ProtocolName, "ProtocolName");
        
        Wt::Dbo::field(a, AcquisitionTime, "AcquisitionTime");
        Wt::Dbo::field(a, AcquisitionDate, "AcquisitionDate");
        Wt::Dbo::field(a, MRSystem, "MRSystem");
        Wt::Dbo::field(a, AccessionNumber, "AccessionNumber");
        Wt::Dbo::field(a, SelectedYarraServer, "SelectedYarraServer");
    }

    MeasFileMeta() : LastWriteTime(0), LastSeen(0) {}

    MeasFileMeta(const boost::filesystem::path& filepath)
        : LastSeen(std::chrono::system_clock::to_time_t(std::chrono::system_clock::now()))
        , Filename(filepath.filename().string())
        , Path(mountPoints.substMountPath(filepath.parent_path().string()))
        , LastWriteTime(boost::filesystem::last_write_time(filepath))
    { }

    // Read the tags from the file.
    // Since the operation is time-consuming, it shall be performed for new or updated files only
    void readMeasFile(const std::string & filename) {
        LOG(INFO) << "Reading measurement file: " << filename;

        SyngoWrapper syngoWrapper(filename.c_str());

        std::ostringstream log_stream;
        TagsLookupTable::tag_variables_t tag_variables;
        bool success = syngoWrapper.calcDcmtagVariables(tag_variables, log_stream);

        PatientName = tag_variables["PatientName"];

        success &= syngoWrapper.getTagValue({"XProtocol.**.RECOMPOSE.PatientID", "XProtocol.**.RECOMPOSE.tPatientID"},
                                            PatientID, log_stream);
        success &= syngoWrapper.getTagValue("XProtocol.**.DICOM.tProtocolName", ProtocolName, log_stream);

        AcquisitionTime = tag_variables["studytime"];
        AcquisitionDate = tag_variables["daystr"];

        if (!success) {
            LOG(WARNING) << "Not all tags were read successfully: " << log_stream.str();
        }

        boost::filesystem::path filepath(filename);
        auto taskFilename = (filepath.parent_path() / filepath.stem()).string() + ".task";

        boost::property_tree::ptree taskFile;
        try {
            boost::property_tree::ini_parser::read_ini(taskFilename, taskFile);

            MRSystem = taskFile.get<std::string>("Information.SystemName");
            AccessionNumber = taskFile.get<std::string>("Task.ACC");
            SelectedYarraServer = taskFile.get<std::string>("Information.SelectedServer");
        } catch (const boost::exception & e) {
            LOG(WARNING) << "Cannot read task file `" << taskFilename << "`: " << boost::diagnostic_information(e) << std::endl;
        }
    }
};
MountPoints MeasFileMeta::mountPoints;

//Check if an index entry already exists and set its Seen Flag if so
//Needs only Filename, Path and LastWriteTime to be provided
bool isMetaIndexed(Wt::Dbo::Session & session, const MeasFileMeta & meta) {
    LOG(INFO) << "Looking up the metadata for " << meta.Path << "/" << meta.Filename;
    Wt::Dbo::Transaction transaction(session);
    Wt::Dbo::ptr<MeasFileMeta> measFileMeta = session.find<MeasFileMeta>().
            where("Filename = ?").bind(meta.Filename).
            where("Path = ?").bind(meta.Path).
            where("LastWriteTime = ?").bind(meta.LastWriteTime);
    if (measFileMeta) {
        //"last seen" update
        measFileMeta.modify()->LastSeen = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
        return true;
    }
    return false;
}

//Insert a new index entry. Needs all attributes to be set
void insertMeta(Wt::Dbo::Session & session, Wt::Dbo::ptr<MeasFileMeta> measFileMeta) {
    LOG(INFO) << "Inserting a record for " << measFileMeta->Path << "/" << measFileMeta->Filename;
    Wt::Dbo::Transaction transaction(session);
    session.add(measFileMeta);
}

//Drop all index entries not seen or updated during last indexing
void dropNotSeen(Wt::Dbo::Session & session, std::time_t dropBefore) {
    Wt::Dbo::Transaction transaction(session);
    session.execute("DELETE FROM measFileIndex WHERE LastSeen < ? ").bind(dropBefore);
}

//Recursively loops through the directories to be indexed and checks files with measFileExtension
void loopOverDirs(Wt::Dbo::Session & session, const std::vector<std::string> & indexed_dirs, const std::string & measFileExtension) {

    for (const auto & indexed_dir : indexed_dirs) {

        boost::filesystem::path dir_path(indexed_dir);
        if (boost::filesystem::is_directory(dir_path)) {
            try {
                for (const auto &dir_entry : boost::filesystem::recursive_directory_iterator(dir_path)) {
                    if (dir_entry.path().extension() == measFileExtension) {

                        try {
                            Wt::Dbo::ptr<MeasFileMeta> measFileMeta(new MeasFileMeta(dir_entry.path()));
                            std::cout << "Processing " << dir_entry.path();
                            if (!isMetaIndexed(session, *measFileMeta)) {
                                std::cout << " -> new entry" << std::endl;
                                measFileMeta.modify()->readMeasFile(dir_entry.path().string());
                                insertMeta(session, measFileMeta);
                            }
                            else {
                                std::cout << " -> existing entry" << std::endl;
                            }
                        } catch (const std::runtime_error &e) {
                            LOG(ERROR) << "Exception when reading " << dir_entry.path() << ": " << e.what();
                        }

                    }
                }
            } catch (const boost::filesystem::filesystem_error & e) {
                LOG(ERROR) << "Exception when recursively iterating " << dir_path << ": " << boost::diagnostic_information(e);
            }
        } else {
            LOG(ERROR) << "The path `" << dir_path << "` doesn't exists, is not accessible or is not a directory.";
        }
    }
}

//Read command line and config file options
bool readOptions(int argc, char *argv[], boost::program_options::variables_map & vm) {
    boost::program_options::options_description generic_options("General options");
    generic_options.add_options()
            ("help,h", "produce help message")
            ("index,I", boost::program_options::value<std::string>()->default_value(std::string(argv[0]) + ".db"),
             "index file name")
            ("keep,k", boost::program_options::value<int>()->default_value(30), "days to keep an index entry before removing")
            ("ext,E", boost::program_options::value<std::string>()->default_value(".dat"), "measurement file extension")
            ("reset", "reset the index database and rebuild the index")
            ("config,C", boost::program_options::value<std::string>(), "config file name");

    boost::program_options::options_description logging_options("Logging options");
    logging_options.add_options()
            ("logtostderr", boost::program_options::value<bool>()->default_value(false),
             "Log messages to stderr instead of logfiles. "
                     "Note: you can set binary flags to true by specifying 1, true, or yes (case insensitive). "
                     "Also, you can set binary flags to false by specifying 0, false, or no (again, case insensitive)")
            ("stderrthreshold", boost::program_options::value<int>()->default_value(2),
             "Copy log messages at or above this level to stderr in addition to logfiles. "
                     "The numbers of severity levels INFO, WARNING, ERROR, and FATAL are 0, 1, 2, and 3, respectively.")
            ("minloglevel", boost::program_options::value<int>()->default_value(0),
             "Log messages at or above this level. "
                     "Again, the numbers of severity levels INFO, WARNING, ERROR, and FATAL are 0, 1, 2, and 3, respectively.")
            ("log_dir", boost::program_options::value<std::string>(),
             "If specified, logfiles are written into this directory instead of the default logging directory.");

    boost::program_options::options_description cmdline_options("Allowed options");
    cmdline_options.add_options()
            ("dirs", boost::program_options::value<std::vector<std::string>>(), "directories to be indexed");
    cmdline_options.add(generic_options).add(logging_options);

    boost::program_options::positional_options_description positional_options;
    positional_options.add("dirs", -1);

    boost::program_options::store(boost::program_options::command_line_parser(argc, argv)
              .options(cmdline_options).positional(positional_options).run(), vm);

    if (vm.count("help")) {
        boost::program_options::options_description visible_options("Allowed options");
        visible_options.add(generic_options).add(logging_options);

        std::cout << "Usage: " << STR(PROJECT_NAME) << " [options] [<directories-to-index>]" << std:: endl
            << visible_options << std::endl
                    << std:: endl
            << "The configuration file has INI-like line-based syntax." << std:: endl
            << "A line in the form:" << std:: endl
            << "    name=value" << std:: endl
            << "gives a value to an option." << std:: endl
            << "The # character introduces a comment that spans until the end of the line." << std:: endl
                    << std:: endl
            << "Options are the same as listed above for the command line (used without --)." << std:: endl
            << "To provide directory paths to index, add one or more lines:" << std:: endl
            << "    index_dir = <path>" << std:: endl
            ;
        return false;
    }

    std::string config_file;
    if (vm.count("config")) {
        config_file = vm["config"].as<std::string>();
        if (!boost::filesystem::is_regular_file(boost::filesystem::path(config_file))) {
            LOG(ERROR) << "The config file `" << config_file << "` doesn't exist";
            return false;
        }
    } else {
        config_file = std::string(argv[0]) + ".ini";
        if (!boost::filesystem::is_regular_file(boost::filesystem::path(config_file))) {
            LOG(WARNING) << "Default config file `" << config_file << "` doesn't exist";
            config_file.erase();
        }
    }

    boost::program_options::options_description config_file_options("Allowed options");
    config_file_options.add_options()
            ("index_dir", boost::program_options::value<std::vector<std::string>>(),
             "directories to be indexed");
    config_file_options.add(generic_options).add(logging_options);

    if (!config_file.empty()) {
        try {
            boost::program_options::store(
                    boost::program_options::parse_config_file<char>(config_file.c_str(), config_file_options), vm, true);
        } catch (const boost::program_options::error & e) {
            LOG(ERROR) << "Error reading config file `" << config_file << "`: " << e.what();
            return false;
        }
    }

    boost::program_options::notify(vm);
    return true;
}


int main(int argc, char *argv[]) {
    //Speed up report output
    std::ios_base::sync_with_stdio(false);

    std::cout << STR(PROJECT_NAME) << " v." << STR(PROJECT_VERSION) << "; build " << __DATE__ << " " << __TIME__
        << std::endl << std::endl;

    try {

        boost::program_options::variables_map vm;
        if (!readOptions(argc, argv, vm)) {
            return 1;
        }

        // Initialize Google's logging library.
        if (vm.count("logtostderr")) {
            FLAGS_logtostderr = vm["logtostderr"].as<bool>();
        }
        if (vm.count("stderrthreshold")) {
            FLAGS_stderrthreshold = vm["stderrthreshold"].as<int>();
        }
        if (vm.count("minloglevel")) {
            FLAGS_minloglevel = vm["minloglevel"].as<int>();
        }
        if (vm.count("log_dir")) {
            FLAGS_log_dir = vm["log_dir"].as<std::string>();
        }
        google::InitGoogleLogging(argv[0]);

        LOG(INFO) << "Starting the indexer";
        
        auto indexDB = vm["index"].as<std::string>();

        //Reset the index database if requested
        if (vm.count("reset")) {
            if (boost::filesystem::is_regular_file(boost::filesystem::path(indexDB))) {
                LOG(INFO) << "Deleting the database file `" << indexDB <<"`";
                boost::filesystem::remove(boost::filesystem::path(indexDB));
            }
            LOG(WARNING) << "Resetting of the database file `" << indexDB << "` was requested but it didn't exist";
        }

        //Init the db connection
        Wt::Dbo::backend::Sqlite3 sqlite3(indexDB);
        Wt::Dbo::Session session;
        session.setConnection(sqlite3);

        //Create table and db index
        session.mapClass<MeasFileMeta>("measFileIndex");
        try {
            Wt::Dbo::Transaction transaction(session);
            session.createTables();
            session.execute("CREATE INDEX index_idx_measFileIndex_filepath on measFileIndex (Filename, Path, LastWriteTime);");
        } catch (const Wt::Dbo::Exception & e) {
            LOG(INFO) << "No tables have been created: " << e.what();
        }

        //Get directory list and loop through it
        std::vector<std::string> indexed_dirs;
        if (vm.count("dirs")) {
            auto dirs_from_cmd = vm["dirs"].as<std::vector<std::string>>();
            indexed_dirs.insert(indexed_dirs.end(), dirs_from_cmd.begin(), dirs_from_cmd.end());
        }
        if (vm.count("index_dir")) {
            auto dirs_from_cfg = vm["index_dir"].as<std::vector<std::string>>();
            indexed_dirs.insert(indexed_dirs.end(), dirs_from_cfg.begin(), dirs_from_cfg.end());
        }
        if (!indexed_dirs.empty()) {
            //Calculate it before looping through directories since it can be time consuming
            typedef std::chrono::duration<int, std::ratio<3600*24>> days;
            std::time_t dropBefore = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now() - days(vm["keep"].as<int>()));
            loopOverDirs(session, indexed_dirs, vm["ext"].as<std::string>());
            dropNotSeen(session, dropBefore);
        } else {
            LOG(ERROR) << "No directories provided";
        }

    } catch (const boost::exception & e) {
        LOG(ERROR) << "boost::exception: " << boost::diagnostic_information(e);
        return 1;
    } catch (const std::runtime_error & e) {
        LOG(ERROR) << "std::runtime_error: " << e.what();
        return 1;
    }

    LOG(INFO) << "Stopping the indexer with return code 0";
    return 0;
}
